import React, { Component, PropTypes } from 'react';
import Product from './Product';
import Filter from './Filter';
import {img1,img2,img3,img4,img5,img6,img7,img8,img9,img10} from './images/index';


class Home extends Component {
  constructor(props){
    super(props);
    this.handleProduct = this.handleProduct.bind(this);

    this.state = {
      products:{
        arrayOfProducts:[
        {
          name:'CHECK PRINT SHIRT',
          src:img1,
          size:'S',
          check: true
        },
        {
            name:'GLORIA HIGH LOGO SNEAKER',
            src:img2,
            size:'L',
            check: true
          },
          {
            name:'CATE RIGID BAG',
            src:img3,
            size:'S',
            check: true
          },
          {
            name:'GUESS CONNECT WATCH',
            src:img4,
            size:'L',
            check: true
          },
          {
            name:'70s RETRO GLAM KEFIAH',
            src:img5,
            size:'L',
            check: true
          },
          {
            name:'Generic Hiamok Women Sleeveless',
            src:img6,
            size:'S',
            check: true
          },
          {
            name:'Generic Stylish Women Button Coat ',
            src:img7,
            size:'S',
            check: true
          },
          {
            name:'Fashion Plus Size Mens Jacket',
            src:img8,
            size:'L',
            check: true
          },
          {
            name:'Giro Stripe Giro Active Wear',
            src:img9,
            size:'L',
            check: true
          },
          {
            name:'Generic Slim Jean Pants',
            src:img10,
            size:'S',
            check: true
          }
      ]}
    };
  }
  handleProduct(ev){
      let products = this.state.products.arrayOfProducts;

    products.map((p)=>{

if (p.size === ev || ev === "A") {
 p.check= true;
}else{
  p.check= false;
}


});
    console.log(products);
    this.setState({products:
          {arrayOfProducts:products}
    });
  }
  render() {
    return (

        <div className="panel  panel-info">
  <div className="panel-heading">
    <h3 className="panel-title">Our products</h3>
    <Filter  filter={this.state.products} onSelectFilter={this.handleProduct}/>
  </div>
  <div className="panel-body">
    <div className="row">
    <Product items={this.state.products}  />
    </div>
  </div>
</div>


    );
  }
}

export default Home;
