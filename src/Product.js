import React, { Component } from 'react';
class Product extends Component {

  render(){
    let products = this.props.items.arrayOfProducts;
    let productsMap = products.map((p,i)=>{
      let name= p.name;
      let src= p.src;
      let size= p.size;
      let check= p.check;
      return(
        <div key={i} className={p.check? "" : "hidden"}>
        <div className="col-md-3">
          <div className="cont">
        <div className="products-cont">
        <div className="products-img">
        <img src={src} alt="" />
        </div>
        <div className="products-details">
        <h5>{name}:</h5>
        <p><b>Size: </b><span>{size}</span></p>
        <button className="btn btn-info btn-xs">Read more</button>
        </div>
        </div>
        </div>
        </div>
        </div>
      );
    })
  return productsMap;

  }
}
export default Product;
